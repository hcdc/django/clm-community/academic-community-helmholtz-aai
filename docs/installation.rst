.. SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _installation:

Installation
============

.. warning::

   This page has been automatically generated as has not yet been reviewed by the
   authors of academic-community-helmholtz-aai!

To install the `academic-community-helmholtz-aai` package for your Django
project, you need to follow two steps:

1. :ref:`Install the package <install-package>`
2. :ref:`Add the app to your Django project <install-django-app>`


.. _install-package:

Installation from PyPi
----------------------
The recommended way to install this package is via pip and PyPi via::

    pip install academic-community-helmholtz-aai

or, if you are using `pipenv`, via::

    pipenv install academic-community-helmholtz-aai

Or install it directly from `the source code repository on Gitlab`_ via::

    pip install git+https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai.git

The latter should however only be done if you want to access the development
versions (see :ref:`install-develop`).


.. _install-django-app:

Install the Django App for your project
---------------------------------------
To use the `academic-community-helmholtz-aai` package in your Django project,
you need to add the app to your ``INSTALLED_APPS``, configure your ``urls.py``, run
the migration, add a login button in your templates. Here are the step-by-step
instructions:

1. Add the ``academic_community_helmholtz_aai`` app to your ``INSTALLED_APPS``
2. in your projects urlconf (see :setting:`ROOT_URLCONF`), add include
   :mod:`academic_community_helmholtz_aai.urls` via::

       from django.urls import include, path

       urlpatterns += [
           path("academic-community-helmholtz-aai/", include("academic_community_helmholtz_aai.urls")),
        ]
3. Run ``python manage.py migrate`` to add models to your database
4. Configure the app to your needs (see :ref:`configuration`) (you can also
   have a look into the ``testproject`` folder in the `source code repository`_
   for a possible configuration)

   Please make sure that you have an ``asgi.py`` file as in the
   ``testproject`` and set the ``ASGI_APPLICATION`` variable in your
   ``settings.py``.
   See https://channels.readthedocs.io/en/stable/installation.html


If you need a deployment-ready django setup for an app like this, please
have a look at the following template:

https://codebase.helmholtz.cloud/hcdc/software-templates/djac-docker-template


That's it!

.. _the source code repository on Gitlab: https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai


.. _install-develop:

Installation for development
----------------------------
Please head over to our :ref:`contributing guide <contributing>` for
installation instruction for development.

.. SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _api:

API Reference
=============

.. toctree::
    :maxdepth: 1

    api/academic_community_helmholtz_aai.app_settings
    api/academic_community_helmholtz_aai.urls
    api/academic_community_helmholtz_aai.models
    api/academic_community_helmholtz_aai.views


.. toctree::
    :hidden:

    api/modules

.. SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. academic-community-helmholtz-aai documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to academic-community-helmholtz-aai's documentation!
============================================================

|CI|
|Code coverage||Docs|
|Latest Release|
|PyPI version|
|Code style: black|
|Imports: isort|
|PEP8|
|Checked with mypy|
|REUSE status|

.. rubric:: Utility app for connecting a community platform with the Helmholtz AAI

.. warning::

    This page has been automatically generated as has not yet been reviewed by
    the authors of academic-community-helmholtz-aai!
    Stay tuned for updates and discuss with us at
    https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   api
   contributing


How to cite this software
-------------------------

.. card:: Please do cite this software!

   .. tab-set::

      .. tab-item:: APA

         .. citation-info::
            :format: apalike

      .. tab-item:: BibTex

         .. citation-info::
            :format: bibtex

      .. tab-item:: RIS

         .. citation-info::
            :format: ris

      .. tab-item:: Endnote

         .. citation-info::
            :format: endnote

      .. tab-item:: CFF

         .. citation-info::
            :format: cff


License information
-------------------
Copyright © 2022-2024 Helmholtz-Zentrum hereon GmbH

The source code of academic-community-helmholtz-aai is licensed under
EUPL-1.2.

If not stated otherwise, the contents of this documentation is licensed under
CC-BY-4.0.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. |CI| image:: https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai/badges/main/pipeline.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai/-/pipelines?page=1&scope=all&ref=main
.. |Code coverage| image:: https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai/badges/main/coverage.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai/-/graphs/main/charts
.. TODO: uncomment the following line when the documentation is published at https://readthedocs.org
.. .. |Docs| image:: https://readthedocs.org/projects/academic-community-helmholtz-aai/badge/?version=latest
..    :target: https://django-academic-community.readthedocs.io/projects/academic-community-helmholtz-aai/en/latest/
.. |Latest Release| image:: https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai/-/badges/release.svg
   :target: https://codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai
.. .. TODO: uncomment the following line when the package is published at https://pypi.org
.. .. |PyPI version| image:: https://img.shields.io/pypi/v/academic-community-helmholtz-aai.svg
..    :target: https://pypi.python.org/pypi/academic-community-helmholtz-aai/
.. |Code style: black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. |Imports: isort| image:: https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336
   :target: https://pycqa.github.io/isort/
.. |PEP8| image:: https://img.shields.io/badge/code%20style-pep8-orange.svg
   :target: https://www.python.org/dev/peps/pep-0008/
.. |Checked with mypy| image:: http://www.mypy-lang.org/static/mypy_badge.svg
   :target: http://mypy-lang.org/
.. TODO: uncomment the following line when the package is registered at https://api.reuse.software
.. .. |REUSE status| image:: https://api.reuse.software/badge/codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai
..    :target: https://api.reuse.software/info/codebase.helmholtz.cloud/hcdc/django/clm-community/academic-community-helmholtz-aai

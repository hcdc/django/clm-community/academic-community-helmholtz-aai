"""Forms
-----

Forms for the academic_community_helmholtz_aai app.
"""

# Copyright (C) 2022 Helmholtz-Zentrum Hereon
# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

from academic_community.institutions.forms import AcademicOrganizationField
from django import forms

from academic_community_helmholtz_aai import models


class VORuleForm(forms.ModelForm):
    class Meta:
        model = models.VORule
        fields = "__all__"

    organization = AcademicOrganizationField(required=False)

# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App settings
------------

This module defines the settings options for the
``academic-community-helmholtz-aai`` app.
"""


from __future__ import annotations

from typing import Optional

from django.conf import settings  # noqa: F401

#: Shall the member information be updated on every login?
HELMHOLTZ_AAI_UPDATE_MEMBERS: bool = getattr(
    settings, "HELMHOLTZ_AAI_UPDATE_MEMBERS", True
)


#: Shall we add new members to a dedicated namepsace? Note that this only
#: affects users that have not communitymember-status
HELMHOLTZ_AAI_COMMUNITYMEMBER_NAMESPACE: Optional[str] = getattr(
    settings, "HELMHOLTZ_AAI_COMMUNITYMEMBER_NAMESPACE", None
)

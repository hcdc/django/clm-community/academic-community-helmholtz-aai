# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Views
-----

Views of the academic-community-helmholtz-aai app to be imported via the url
config (see :mod:`academic_community_helmholtz_aai.urls`).
"""

from __future__ import annotations

from typing import TYPE_CHECKING

from academic_community.history.views import RevisionMixin
from django.contrib import messages

if TYPE_CHECKING:
    from django_helmholtz_aai.models import (
        HelmholtzUser,
        HelmholtzVirtualOrganization,
    )


class CommunityHelmholtzAuthentificationViewMixin(RevisionMixin):
    """Authentification view for the Helmholtz AAI."""

    vo_changed: bool = False

    @property
    def send_success_mail(self):
        """Test if we should send a mail to the managers.

        We only send mails if a new user has been created or if the the
        user left or changed a VO with a VORule.
        """
        return (
            self.is_new_user or self.vo_changed
        ) and super().send_success_mail

    def revision_request_creates_revision(self, request):
        """Test if the request should create a revision."""
        # reimplemented to create revisions with the "GET" method
        return (
            super().revision_request_creates_revision(request)
            or request.method == "GET"
        )

    @property
    def object(self) -> HelmholtzUser:
        """Get the object (required for the success mail)."""
        return self.aai_user

    def get_success_url(self) -> str:
        """Run the get_success_message method to send mails."""
        success_message = self.get_success_message(self.userinfo)
        if success_message:
            messages.success(self.request, success_message)
        return super().get_success_url()

    def leave_vo(self, vo: HelmholtzVirtualOrganization):
        if hasattr(vo, "vorule"):
            """When there exists a VO rule, we should send a mail"""
            self.vo_changed = True
        super().leave_vo(vo)

    def join_vo(self, vo: HelmholtzVirtualOrganization):
        if hasattr(vo, "vorule"):
            """When there exists a VO rule, we should send a mail"""
            self.vo_changed = True
        super().join_vo(vo)

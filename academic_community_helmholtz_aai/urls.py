# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""URL config
----------

URL patterns of the academic-community-helmholtz-aai to be included via::

    from django.urls import include, path

    urlpatters = [
        path(
            "helmholtz-aai",
            include("academic_community_helmholtz_aai.urls"),
        ),
    ]
"""
from __future__ import annotations

from django.urls import path  # noqa: F401
from django_helmholtz_aai.views import registry

from academic_community_helmholtz_aai import views  # noqa: F401

#: App name for the academic-community-helmholtz-aai to be used in calls to
#: :func:`django.urls.reverse`
app_name = "academic_community_helmholtz_aai"

viewset = registry.get_default_viewset()


class AuthentificationView(
    views.CommunityHelmholtzAuthentificationViewMixin,
    viewset.AuthentificationView,  # type: ignore[name-defined]
):
    pass


viewset.AuthentificationView = AuthentificationView

if hasattr(viewset, "UserLinkingConfirmView"):

    class UserLinkingConfirmView(
        views.CommunityHelmholtzAuthentificationViewMixin,
        viewset.UserLinkingConfirmView,  # type: ignore[name-defined]
    ):
        is_new_user = True

    viewset.UserLinkingConfirmView = UserLinkingConfirmView

#: urlpattern for the Helmholtz AAI
urlpatterns = viewset.get_urls()

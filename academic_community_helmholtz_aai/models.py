# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Models
------

Models for the academic-community-helmholtz-aai app.
"""

from __future__ import annotations

import datetime as dt
from typing import TYPE_CHECKING, Dict, Optional, Tuple, Union

import reversion
from academic_community.channels.models import (
    GroupMentionLink,
    ManualUserMentionLink,
    UserMentionLink,
)
from academic_community.channels.models.groups import create_default_groups
from academic_community.channels.models.subscriptions import (
    create_chat_settings,
)
from academic_community.institutions.models import (
    AcademicMembership,
    AcademicOrganization,
)
from django.conf import settings
from django.contrib import messages
from django.db import models  # noqa: F401
from django.db.models.signals import post_delete, post_save, pre_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.safestring import mark_safe
from django_helmholtz_aai import signals
from django_helmholtz_aai.models import (
    HelmholtzUser,
    HelmholtzVirtualOrganization,
)

from academic_community_helmholtz_aai import app_settings  # noqa: F401

if TYPE_CHECKING:
    from academic_community.members.models import CommunityMember
    from django.contrib.auth.models import User


RULE_CACHE: Dict[int, Dict[str, Union[None, bool, int]]] = {}


GroupMentionLink.registry.register_autocreation(HelmholtzVirtualOrganization)(
    GroupMentionLink
)
UserMentionLink.registry.register_autocreation(HelmholtzUser)(UserMentionLink)
ManualUserMentionLink.registry.register_autocreation(HelmholtzUser)(
    ManualUserMentionLink
)


@reversion.register
class VORule(models.Model):
    """A rule to apply for the Helmholtz AAI."""

    vo = models.OneToOneField(
        HelmholtzVirtualOrganization,
        on_delete=models.CASCADE,
        help_text="The VO that these rules apply for",
    )

    is_member = models.BooleanField(
        default=False,
        help_text="Shall all members of these VO be community members?",
    )

    is_manager = models.BooleanField(
        default=False,
        help_text="Shall all members of the VO be community managers?",
    )

    organization = models.ForeignKey(
        AcademicOrganization,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text="Shall all members of this VO be added to an academic organization?",
    )

    def __str__(self) -> str:
        return str(self.vo)


@receiver(post_delete, sender=VORule)
def unapply_vorule(instance: VORule, **kwargs):
    """Unapply all rules for the VO"""
    if not (
        instance.is_member or instance.is_manager or instance.organization
    ):
        return
    vo = instance.vo
    for user in vo.user_set.all():
        if instance.is_member:
            remove_member(user, vo)
        if instance.is_manager:
            remove_manager(user, vo)
        if instance.organization and hasattr(user, "communitymember"):
            member: CommunityMember = user.communitymember  # type: ignore
            memberships = member.active_memberships.filter(
                organization=instance.organization
            )
            if memberships:
                _end_academicmemberships(memberships, instance.organization)


@receiver(pre_save, sender=VORule)
def store_update(instance: VORule, **kwargs):
    """Store updated information"""
    if not hasattr(instance, "pk"):
        return
    try:
        current_rule = VORule.objects.get(pk=instance.pk)
    except VORule.DoesNotExist:
        return
    updates: Dict[str, Union[None, bool, int]] = {}
    if current_rule.is_member != instance.is_member:
        updates["is_member"] = current_rule.is_member
    if current_rule.is_manager != instance.is_manager:
        updates["is_manager"] = current_rule.is_manager
    current_orga = current_rule.organization
    orga = instance.organization
    if (current_orga and current_orga.pk) != (orga and orga.pk):
        updates["organization"] = getattr(current_orga, "pk", None)
    if updates:
        RULE_CACHE[instance.pk] = updates


@receiver(post_save, sender=VORule)
def update_after_create(instance: VORule, created: bool, **kwargs):
    """Update the users when a rule has been created."""
    if not created:
        return
    vo = instance.vo
    for user in vo.user_set.all():
        if instance.is_member:
            add_member_from_vo(user, vo, force=True)
        if instance.is_manager:
            add_manager_from_vo(user, vo, force=True)
        if instance.organization and hasattr(user, "communitymember"):  # type: ignore
            if not user.communitymember.is_member_of(instance.organization):
                _add_academicmembership(
                    user.communitymember, instance.organization
                )


@receiver(post_save, sender=VORule)
def update_rules(instance: VORule, **kwargs):
    """Update users after rule change."""

    def orga_isin(orga, orgas):
        return orga.pk in (orga.pk for orga in orgas)

    updates = RULE_CACHE.pop(instance.pk, {})
    if not updates:
        return
    vo = instance.vo
    for user in vo.user_set.all():
        if "is_member" in updates:
            if instance.is_member:
                add_member_from_vo(user, vo, force=True)
            else:
                remove_member(user, vo, force=True)
        if "is_manager" in updates:
            if instance.is_manager:
                add_manager_from_vo(user, vo, force=True)
            else:
                remove_manager(user, vo, force=True)
        if "organization" in updates and hasattr(user, "communitymember"):
            orga_pk = updates["organization"]
            member: CommunityMember = user.communitymember  # type: ignore
            new_orga = instance.organization
            if orga_pk is not None:
                try:
                    orga = AcademicOrganization.objects.get(pk=orga_pk)
                except AcademicOrganization.DoesNotExist:
                    pass
                else:
                    memberships = member.active_memberships.filter(
                        organization=orga
                    )
                    if new_orga and new_orga.pk == orga.pk:
                        pass
                    elif new_orga and (
                        orga_isin(new_orga, orga.parent_organizations)
                        or orga_isin(new_orga, orga.sub_organizations)
                    ):
                        for ms in memberships:
                            ms.organization = new_orga  # type: ignore
                            ms.save()
                        return
                    else:
                        _end_academicmemberships(memberships, orga)
            if new_orga:
                if not member.is_member_of(new_orga):  # type: ignore
                    _add_academicmembership(member, new_orga)


@receiver(signals.aai_vo_entered)
def add_member_from_vo(user: User, vo: HelmholtzVirtualOrganization, **kwargs):
    """Add members from a VO."""

    from academic_community.members.models import (
        MembersConfig,  # type: ignore[attr-defined]
    )

    if not hasattr(vo, "vorule") or not hasattr(user, "communitymember"):
        return
    member: CommunityMember = user.communitymember  # type: ignore
    force: bool = kwargs.pop("force", False)
    vorule: VORule = vo.vorule  # type: ignore
    if (force or vorule.is_member) and not member.is_member:
        with reversion.create_revision():
            reversion.set_user(user)
            reversion.set_comment(
                "Update member status of {user}\n\n"
                "because {user} entered {vo}"
            )
            member.is_member = True
            if getattr(settings, "COMMUNITYMEMBER_NAMESPACE", None):
                member.app_config = MembersConfig.objects.get(
                    namespace=settings.COMMUNITYMEMBER_NAMESPACE  # type: ignore[misc]
                )
            member.save()


@receiver(signals.aai_vo_left)
def remove_member(user: User, vo: HelmholtzVirtualOrganization, **kwargs):
    """Remove the is_member status when they leave the vo."""

    from academic_community.members.models import MembersConfig

    if not hasattr(vo, "vorule") or not hasattr(user, "communitymember"):
        return
    member: CommunityMember = user.communitymember  # type: ignore
    force: bool = kwargs.pop("force", False)
    vorule: VORule = vo.vorule  # type: ignore
    if (force or vorule.is_member) and member.is_member:
        if not user.groups.filter(
            helmholtzvirtualorganization__vorule__is_member=True
        ):
            with reversion.create_revision():
                reversion.set_user(user)
                reversion.set_comment(
                    "Update member status of {user}\n\n"
                    "because {user} left {vo}"
                )
                member.is_member = False
                if app_settings.HELMHOLTZ_AAI_COMMUNITYMEMBER_NAMESPACE:
                    member.app_config = MembersConfig.objects.get(
                        namespace=app_settings.HELMHOLTZ_AAI_COMMUNITYMEMBER_NAMESPACE
                    )
                member.save()


@receiver(signals.aai_vo_entered)
def add_manager_from_vo(
    user: User, vo: HelmholtzVirtualOrganization, **kwargs
):
    """Add managers from a VO."""
    from academic_community.utils import get_managers_group

    if not hasattr(vo, "vorule") or user.is_manager:  # type: ignore
        return
    force: bool = kwargs.pop("force", False)
    vorule: VORule = vo.vorule  # type: ignore
    if (force or vorule.is_manager) and not user.is_manager:  # type: ignore
        user.groups.add(get_managers_group())


@receiver(signals.aai_vo_left)
def remove_manager(user: User, vo: HelmholtzVirtualOrganization, **kwargs):
    """Remove the is_member status when they leave the vo."""
    from academic_community.utils import get_managers_group

    if not hasattr(vo, "vorule") or not user.is_manager:  # type: ignore
        return
    force: bool = kwargs.pop("force", False)
    vorule: VORule = vo.vorule  # type: ignore
    if (force or vorule.is_manager) and user.is_manager:  # type: ignore
        if not user.groups.filter(
            helmholtzvirtualorganization__vorule__is_manager=True
        ):
            user.groups.remove(get_managers_group())


def _add_academicmembership(
    member: CommunityMember, orga: AcademicOrganization
):
    """Create an academic membership."""

    parent_pks = [parent.pk for parent in orga.parent_organizations]
    if not parent_pks:
        AcademicMembership.objects.create(member=member, organization=orga)
    else:
        memberships = member.active_memberships
        existing_ms = next(
            (ms for ms in memberships if ms.organization.pk in parent_pks),
            None,
        )
        if existing_ms is None:
            AcademicMembership.objects.create(member=member, organization=orga)
        else:
            existing_ms.organization = orga
            existing_ms.save()


@receiver(signals.aai_vo_entered)
def add_academicmembership(
    user: HelmholtzUser, vo: HelmholtzVirtualOrganization, **kwargs
):
    """Add an academic membership for the user."""
    if (
        not hasattr(vo, "vorule")
        or vo.vorule.organization is None  # type: ignore
        or not hasattr(user, "communitymember")
    ):
        return
    member: CommunityMember = user.communitymember  # type: ignore
    orgas = member.active_organizations
    orga: AcademicOrganization = vo.vorule.organization  # type: ignore
    if orga in orgas:
        return
    with reversion.create_revision():
        reversion.set_user(user)
        reversion.set_comment(
            f"Update academic membership for {user} in {orga} based on {vo}"
        )
        _add_academicmembership(member, orga)


def _end_academicmemberships(
    memberships: models.QuerySet[AcademicMembership],
    orga: AcademicOrganization,
):
    """End the memberships of shift to another one"""

    def orga_key(orga):
        if hasattr(orga, "unit"):
            return 1
        if hasattr(orga, "department"):
            return 2
        if hasattr(orga, "institution"):
            return 3

    user: User = memberships.first().member.user  # type: ignore

    other_orgas: Tuple[int]
    other_orgas = user.groups.filter(  # type: ignore
        helmholtzvirtualorganization__vorule__organization__isnull=False
    ).values_list("helmholtzvirtualorganization__vorule__organization")
    if other_orgas:
        other_orgas = other_orgas[0]  # type: ignore

    parent_pks = [parent.pk for parent in orga.parent_organizations]
    if not parent_pks:
        for ms in memberships:
            ms.end_date = dt.date.today()
            ms.save()
    else:
        overlap = [orga for orga in other_orgas if orga in parent_pks]
        if not overlap:
            for ms in memberships:
                ms.end_date = dt.date.today()
                ms.save()
        else:
            alternative_orgas = [
                AcademicOrganization.objects.get(pk=pk) for pk in overlap
            ]
            lowest = sorted(alternative_orgas, key=orga_key)[0]
            for ms in memberships:
                ms.organization = lowest
                ms.save()


@receiver(signals.aai_vo_left)
def end_academicmembership(
    user: HelmholtzUser, vo: HelmholtzVirtualOrganization, **kwargs
):
    """End an academic membership when the user leaves the VO."""

    if (
        not hasattr(vo, "vorule")
        or vo.vorule.organization is None  # type: ignore
        or not hasattr(user, "communitymember")
    ):
        return
    member: CommunityMember = user.communitymember  # type: ignore
    orgas = member.active_organizations
    orga: AcademicOrganization = vo.vorule.organization  # type: ignore
    if orga.pk not in (orga.pk for orga in orgas):
        return
    memberships = member.active_memberships.filter(organization=orga)
    if not memberships:
        return

    with reversion.create_revision():
        reversion.set_user(user)
        reversion.set_comment(
            f"End academic membership for {user} in {orga} based on {vo}"
        )
        _end_academicmemberships(memberships, orga)


@receiver(signals.aai_vo_left)
def remove_academicmembership(
    user: HelmholtzUser, vo: HelmholtzVirtualOrganization, **kwargs
):
    """Add an academic membership for the user."""
    if (
        not hasattr(vo, "vorule")
        or vo.vorule.organization is None  # type: ignore
        or not hasattr(user, "communitymember")
    ):
        return
    member: CommunityMember = user.communitymember  # type: ignore
    orga: AcademicOrganization = vo.vorule.organization  # type: ignore
    if orga in member.active_organizations:
        AcademicMembership.objects.create(member=member, organization=orga)


@receiver(signals.aai_user_created)
def create_community_member(
    user: HelmholtzUser, request, userinfo: Dict, **kwargs
):
    """Create the CommunityMember for the new user."""
    from academic_community.members.models import (
        CommunityMember,
        Email,
        MembersConfig,
    )
    from academic_community.notifications.models import SystemNotification

    if not user.is_active:
        return

    if userinfo.get("eduperson_entitlement"):
        is_member = HelmholtzVirtualOrganization.objects.filter(
            eduperson_entitlement__in=userinfo["eduperson_entitlement"],
            vorule__is_member=True,
        ).exists()
    else:
        is_member = False

    app_config: Optional[str] = None

    if is_member:
        if getattr(settings, "COMMUNITYMEMBER_NAMESPACE", None):
            app_config = MembersConfig.objects.get(
                namespace=settings.COMMUNITYMEMBER_NAMESPACE  # type: ignore[misc]
            )
        elif app_settings.HELMHOLTZ_AAI_COMMUNITYMEMBER_NAMESPACE:
            app_config = MembersConfig.objects.get(
                namespace=app_settings.HELMHOLTZ_AAI_COMMUNITYMEMBER_NAMESPACE
            )

    with reversion.create_revision():
        reversion.set_user(user)
        reversion.set_comment(f"Create communitymember profile for {user}")
        member = CommunityMember.objects.create(
            first_name=user.first_name,
            last_name=user.last_name,
            user=user,
            is_member=is_member,
            app_config=app_config,
        )
        email = Email.objects.create(
            email=user.email, is_verified=True, member=member
        )
        member.email = email
        member.save()
        user.user_ptr.save()  # type: ignore

    community_name = getattr(settings, "COMMUNITY_NAME", "Academic Community")

    SystemNotification.create_notifications(
        [user],
        f"Welcome to the {community_name}!",
        "academic_community_helmholtz_aai/welcome_notification.html",
        {"communitymember": member},
        request,
    )

    # display a welcome message to the user
    welcome_message = f"Welcome to the {community_name}!"
    if (
        getattr(settings, "COMMUNITY_MEMBER_REGISTRATION", True)
        and not member.is_member
    ):
        uri = reverse("members:self-become-member")
        welcome_message += (
            "<br>"
            f"Click <a href='{uri}' target='_blank'>here</a> "
            "to request community member status."
        )
    messages.success(request, mark_safe(welcome_message))


@receiver(signals.aai_user_updated)
def update_community_member(
    user: HelmholtzUser, to_update: Dict, request, **kwargs
):
    from academic_community.members.models import Email

    if user.is_active and not hasattr(user, "communitymember"):
        create_community_member(user, request, user.userinfo)
        return

    if not app_settings.HELMHOLTZ_AAI_UPDATE_MEMBERS:
        return
    update_vals = {"email", "first_name", "last_name"}
    if not update_vals.intersection(to_update):
        return
    with reversion.create_revision():
        reversion.set_user(user)
        reversion.set_comment(f"Update communitymember profile for {user}")
        member: CommunityMember = user.communitymember  # type: ignore
        if "email" in to_update:
            try:
                email = Email.objects.get(
                    email__iexact=to_update["email"].lower(),
                    member=member,
                    is_verified=True,
                )
            except Email.DoesNotExist:
                email = Email.objects.create(
                    email=to_update["email"], member=member, is_verified=True
                )
            member.email = email
        if "last_name" in to_update:
            member.last_name = to_update["last_name"]
        if "first_name" in to_update:
            member.first_name = to_update["first_name"]

        member.save()


@receiver(post_save, sender=HelmholtzUser)
def _create_chat_settings(instance: HelmholtzUser, **kwargs):
    create_chat_settings(instance.user_ptr, **kwargs)  # type: ignore[attr-defined]


@receiver(post_save, sender=HelmholtzUser)
def _create_default_groups(instance: HelmholtzUser, **kwargs):
    create_default_groups(instance.user_ptr, **kwargs)  # type: ignore[attr-defined]


@receiver(signals.login_denied)
def add_reason_to_message(reason: str, msg: str, request, **kwargs):
    messages.error(request, mark_safe(msg))

# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Admin interfaces
----------------

This module defines the academic-community-helmholtz-aai
Admin interfaces.
"""


from django.contrib import admin  # noqa: F401
from reversion_compare.admin import CompareVersionAdmin

from academic_community_helmholtz_aai import forms, models  # noqa: F401


@admin.register(models.VORule)
class VORuleAdmin(CompareVersionAdmin):
    """The admin for a VO Rule."""

    form = forms.VORuleForm

    autocomplete_fields = ["vo"]

    search_fields = ["vo__name", "vo__eduperson_entitlement"]

    list_display = [
        "vo",
        "is_member",
        "is_manager",
        "organization",
        "users",
        "members",
    ]

    list_editable = ["is_member", "is_manager"]

    list_filter = ["is_member", "is_manager"]

    def users(self, obj: models.VORule):
        return str(obj.vo.user_set.count())

    def members(self, obj: models.VORule):
        return str(
            obj.vo.user_set.filter(communitymember__isnull=False).count()
        )

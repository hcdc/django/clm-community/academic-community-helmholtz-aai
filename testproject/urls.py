# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

"""academic-community-helmholtz-aai URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path  # noqa: F401

urlpatterns = [
    path("helmholtz-aai/", include("academic_community_helmholtz_aai.urls")),
    path("helmholtz-aai/", include("django_helmholtz_aai.urls")),
    path(settings.ADMIN_LOCATION, admin.site.urls),
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    path(settings.ADMIN_LOCATION, admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("", include("academic_community.urls")),
]


# # This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns += static(  # type: ignore
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
    urlpatterns += static(  # type: ignore
        "static/", document_root=settings.STATIC_ROOT
    )

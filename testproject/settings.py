# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

"""
Django settings for the academic-community-helmholtz-aai project.

Generated by 'django-admin startproject' using Django 3.2.15.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

from email.utils import getaddresses
from pathlib import Path
from typing import List

import academic_community
import environ

env = environ.Env()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


READ_DOT_ENV_FILE = env.bool("DJANGO_READ_DOT_ENV_FILE", default=False)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(BASE_DIR / ".env"))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="django-insecure-+_fai#*zi53=1w^y6hdai0spid)bvw2&$p=o952urq(iha2}",
)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool("DEBUG", True)


if env.list("ALLOWED_HOSTS", default=[]):
    ALLOWED_HOSTS: List[str] = env.list("ALLOWED_HOSTS")


# Application definition

INSTALLED_APPS = (
    ["testproject"]
    + academic_community.INSTALLED_APPS
    + [
        "academic_community_helmholtz_aai",
        "django_helmholtz_aai",
        "daphne",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.messages",
        "django.contrib.staticfiles",
        "django.contrib.admindocs",
    ]
)

MIDDLEWARE = academic_community.MIDDLEWARE

ROOT_URLCONF = "testproject.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": academic_community.context_processors
            + [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

# ADMINS="Philipp S. Sommer <philipp.sommer@hereon.de>"
ADMINS = getaddresses(env.list("ADMINS", default=[]))

# MANAGERS="Philipp S. Sommer <philipp.sommer@hereon.de>"
MANAGERS = getaddresses(env.list("MANAGERS", default=[]))

WSGI_APPLICATION = "testproject.wsgi.application"

ASGI_APPLICATION = "testproject.asgi.application"

CHANNELS_REDIS_URL = env("CHANNELS_REDIS_URL", default="")

if CHANNELS_REDIS_URL:
    CHANNEL_LAYERS = {
        "default": {
            "BACKEND": "channels_redis.core.RedisChannelLayer",
            "CONFIG": {
                "hosts": [CHANNELS_REDIS_URL],
            },
        },
    }
else:
    CHANNEL_LAYERS = {
        "default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}
    }


# Email settings
SERVER_EMAIL = env(
    "SERVER_EMAIL",
    default="no-reply.academic-community-helmholtz-aai@example.com",
)

EMAIL_CONFIG = env.email(
    "EMAIL_URL", default="consolemail://user:password@localhost:25"
)

vars().update(EMAIL_CONFIG)

EMAIL_SUBJECT_PREFIX = "[academic-community-helmholtz-aai] "


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

# isort: off
from . import database  # noqa: E402

# isort: on

DATABASES = {"default": database.config(env)}

CACHES = {"default": env.cache(default="dummycache://")}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",  # noqa: E501
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",  # noqa: E501
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",  # noqa: E501
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",  # noqa: E501
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = "en"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = "/static/"

STATIC_ROOT = BASE_DIR / "static"
MEDIA_ROOT = BASE_DIR / "media" / "public"

MEDIA_URL = "/media/"

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

if env.bool("FORCE_HTTPS", default=False):
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True

    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

ADMIN_LOCATION = env("ADMIN_LOCATION", default="admin/")

# DJAC Settings

TEXT_INLINE_EDITING = True

CMS_CONFIRM_VERSION4 = True

# CMS page that holds contact information
CONTACT_PAGE_ID = "contact"

# require authentification for rest API
REST_FRAMEWORK = academic_community.REST_FRAMEWORK


CKEDITOR_SETTINGS_COMMENT = {
    "extraPlugins": ["codesnippet", "emoji", "mentions", "uploadimage"],
    "toolbar_HTMLField": [
        ["Undo", "Redo"],
        ["CodeSnippet", "EmojiPanel"],
        ["ShowBlocks"],
        ["Format", "Styles"],
        ["TextColor", "BGColor", "-", "PasteText", "PasteFromWord"],
        ["Scayt"],
        ["Maximize", ""],
        "/",
        [
            "Bold",
            "Italic",
            "Underline",
            "Strike",
            "-",
            "Subscript",
            "Superscript",
            "-",
            "RemoveFormat",
        ],
        ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"],
        ["HorizontalRule"],
        ["Link", "Unlink"],
        ["NumberedList", "BulletedList"],
        [
            "Outdent",
            "Indent",
            "-",
            "Blockquote",
            "-",
            "Image",
            "-",
            "Link",
            "Unlink",
            "-",
            "Table",
        ],
        ["Source"],
    ],
}


# login to the members profile
LOGIN_REDIRECT_URL = "/accounts/profile/"
LOGIN_URL = "/accounts/login/"

ROOT_URL = env("ROOT_URL", default="http://127.0.0.1:8000")

SITE_ID = 1

PWA_APP_SPLASH_SCREEN = [
    {
        "src": "/static/images/logo-splash_640x1136.png",
        "media": "(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)",
    }
]

VAPID_PUBLIC_KEY = "BOZE2qUwrEBs0LWChxSrAyvHD764-zj1Gl5mDcqyVacv9qpmunPVFgIFs_HETcoHqxZ1WVeQJFgSXBsOPphoPcs"
VAPID_PRIVATE_KEY = "UNaRPJugn3jLPkXfHfFnEaHTp-k_enhrsChYuVghPTE"
VAPID_ADMIN_EMAIL = "no-reply.academic-community@example.com"


AUTHENTICATION_BACKENDS = academic_community.AUTHENTICATION_BACKENDS

HELMHOLTZ_CLIENT_ID = env("HELMHOLTZ_CLIENT_ID", default=None)
HELMHOLTZ_CLIENT_SECRET = env("HELMHOLTZ_CLIENT_SECRET", default=None)


HELMHOLTZ_CREATE_USERS_STRATEGY = [
    "manual-new",
    "map-existing",
    "no-duplicated-helmholtz",
]

COMMUNITY_ABBREVIATION = "CLM"


HELMHOLTZ_UPDATE_EMAIL = False

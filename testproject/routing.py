# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

"""
Websocket routing configuration for the academic-community-helmholtz-aai project.

It exposes the `websocket_urlpatterns` list, a list of url patterns to be used
for deployment.

See Also
--------
https://channels.readthedocs.io/en/stable/topics/routing.html
"""

from typing import Any, List

import academic_community.routing
from channels.routing import URLRouter  # noqa: F401
from django.urls import path  # noqa: F401

websocket_urlpatterns: List[
    Any
] = academic_community.routing.websocket_urlpatterns + [
    # django project specific routes
]

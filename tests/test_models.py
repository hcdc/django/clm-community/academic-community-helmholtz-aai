# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the academic-community-helmholtz-aai models
-----------------------------------------------------
"""
import itertools
from typing import Callable

import pytest
import reversion
from academic_community.institutions.models import (
    Department,
    Institution,
    Unit,
)
from academic_community.members.models import CommunityMember
from django.contrib.auth.middleware import AuthenticationMiddleware
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django_helmholtz_aai.models import (
    HelmholtzUser,
    HelmholtzVirtualOrganization,
)

from academic_community_helmholtz_aai import models


@pytest.fixture
def dummy_password() -> str:
    return "some_password"


@pytest.fixture
def user(db, dummy_password: str) -> HelmholtzUser:
    user = HelmholtzUser(
        username="dummyuser",
        password=dummy_password,
        eduperson_unique_id="someid",
    )
    user.set_password(dummy_password)
    user.save()
    return user


@pytest.fixture
def member(db, dummy_password: str, user: HelmholtzUser) -> CommunityMember:
    with reversion.create_revision():
        member = CommunityMember.objects.create(
            first_name="Max", last_name="Mustermann", user=user
        )
    return member


@pytest.fixture
def institution(db) -> Institution:
    """Generate a test institution."""
    with reversion.create_revision():
        return Institution.objects.create(
            name="some institution", abbreviation="HZG"
        )


@pytest.fixture
def department(institution: Institution) -> Department:
    """Generate a test department."""
    with reversion.create_revision():
        return Department.objects.create(
            name="some department", parent_institution=institution
        )


@pytest.fixture
def unit_factory(department: Department) -> Callable[[], Unit]:
    """A factory for units."""
    counter = itertools.count()

    def factory():
        i = next(counter)
        with reversion.create_revision():
            return Unit.objects.create(
                name=f"some unit{i}", parent_department=department
            )

    return factory


@pytest.fixture
def unit(unit_factory: Callable[[], Unit]) -> Unit:
    """A unit."""
    return unit_factory()


@pytest.fixture
def vo_factory(db) -> Callable[[], HelmholtzVirtualOrganization]:
    """A factory to create virtual organizations"""

    counter = itertools.count()

    def factory() -> HelmholtzVirtualOrganization:
        i = next(counter)
        return HelmholtzVirtualOrganization.objects.create(
            name=f"test:group:testvo{i}",
            eduperson_entitlement=f"test:group:testvo{i}",
        )

    return factory


@pytest.fixture
def vo(
    vo_factory: Callable[[], HelmholtzVirtualOrganization]
) -> HelmholtzVirtualOrganization:
    """A virtual organization."""
    return vo_factory()


@pytest.fixture
def vorule_factory(
    vo_factory: Callable[[], HelmholtzVirtualOrganization]
) -> Callable[..., models.VORule]:
    """A factory to create vorule and VOs."""

    def factory(**kwargs):
        with reversion.create_revision():
            if "vo" not in kwargs:
                kwargs["vo"] = vo_factory()
            return models.VORule.objects.create(**kwargs)

    return factory


def process_request(request):
    session_middleware = SessionMiddleware()
    session_middleware.process_request(request)
    request.session.save()

    auth_middleware = AuthenticationMiddleware()
    auth_middleware.process_request(request)

    message_middleware = MessageMiddleware()
    message_middleware.process_request(request)


@pytest.fixture
def vorule(vorule_factory: Callable[..., models.VORule]) -> models.VORule:
    """A rule for the VO."""
    return vorule_factory()


def test_member_create(user: HelmholtzUser, rf):
    """Test the creation of a member"""
    request = rf.get("/members/")

    process_request(request)

    models.create_community_member(
        user, request=request, userinfo={"eduperson_entitlement": []}
    )

    member = CommunityMember.objects.filter(user=user.pk).first()
    assert member
    assert not member.is_member


def test_member_create_is_member(
    user: HelmholtzUser, rf, vorule_factory: Callable[..., models.VORule]
):
    """Test the creation of a member with a valid VO"""
    request = rf.get("/members/")

    process_request(request)

    vorule = vorule_factory(is_member=True)

    models.create_community_member(
        user,
        request=request,
        userinfo={"eduperson_entitlement": [vorule.vo.eduperson_entitlement]},
    )

    member = CommunityMember.objects.filter(user=user.pk).first()
    assert member
    assert member.is_member


def test_member_update(member: CommunityMember, monkeypatch):
    """Test the creation of a member"""
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.first_name = "test_update"
    user.save()
    models.update_community_member(user, {"first_name": "test_update"}, request=None)  # type: ignore

    member = CommunityMember.objects.get(pk=member.pk)
    assert member.first_name == "test_update"

    user.last_name = "another_test_update"
    user.save()
    models.update_community_member(user, {"last_name": "another_test_update"}, request=None)  # type: ignore

    member = CommunityMember.objects.get(pk=member.pk)
    assert member.last_name == "another_test_update"

    user.email = "test@somewhere.com"
    user.save()
    models.update_community_member(user, {"email": "test@somewhere.com"}, request=None)  # type: ignore

    member = CommunityMember.objects.get(pk=member.pk)
    assert member.email
    assert member.email.email == "test@somewhere.com"


# ----------------------- tests for is_member ---------------------------------


def test_is_member(vorule: models.VORule, member: CommunityMember):
    """Test the creation of members."""
    with reversion.create_revision():
        vorule.is_member = True
        vorule.save()
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.groups.add(vorule.vo)
    models.add_member_from_vo(user, vorule.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    assert member.is_member

    # test leaving the VO
    user.groups.remove(vorule.vo)
    models.remove_member(user, vorule.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    assert not member.is_member


def test_is_member_upon_save(vorule: models.VORule, member: CommunityMember):
    """Test the creation of members upon saving a rule."""
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.groups.add(vorule.vo)
    with reversion.create_revision():
        vorule.is_member = True
        vorule.save()

    member = CommunityMember.objects.get(pk=member.pk)
    assert member.is_member

    with reversion.create_revision():
        vorule.is_member = False
        vorule.save()

    member = CommunityMember.objects.get(pk=member.pk)
    assert not member.is_member


def test_is_member_upon_create(
    vo: HelmholtzVirtualOrganization,
    vorule_factory: Callable[..., models.VORule],
    member: CommunityMember,
):
    """Test the creation of members upon saving a rule."""

    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.groups.add(vo)
    vorule = vorule_factory(vo=vo, is_member=True)

    member = CommunityMember.objects.get(pk=member.pk)
    assert member.is_member

    # test removal
    vorule.delete()

    member = CommunityMember.objects.get(pk=member.pk)
    assert not member.is_member


def test_multiple_is_member(
    vorule_factory: Callable[..., models.VORule], member: CommunityMember
):
    """Test having multiple VOs with is_manager=True"""
    vorule1 = vorule_factory(is_member=True)
    vorule2 = vorule_factory(is_member=True)

    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.groups.add(vorule1.vo, vorule2.vo)
    models.add_member_from_vo(user, vorule1.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    assert member.is_member

    user.groups.remove(vorule1.vo)
    models.remove_member(user, vorule1.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    assert member.is_member

    user.groups.remove(vorule2.vo)
    models.remove_member(user, vorule2.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    assert not member.is_member


# ----------------------- tests for is_manager --------------------------------


def test_is_manager(vorule: models.VORule, member: CommunityMember):
    """Test the creation of members."""
    with reversion.create_revision():
        vorule.is_manager = True
        vorule.save()

    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.groups.add(vorule.vo)
    models.add_manager_from_vo(user, vorule.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    assert user.is_manager  # type: ignore

    # test leaving the VO
    user.groups.remove(vorule.vo)
    models.remove_manager(user, vorule.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    assert not user.is_manager  # type: ignore


def test_is_manager_upon_save(vorule: models.VORule, member: CommunityMember):
    """Test the creation of members upon saving a rule."""

    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.groups.add(vorule.vo)
    with reversion.create_revision():
        vorule.is_manager = True
        vorule.save()

    member = CommunityMember.objects.get(pk=member.pk)
    assert user.is_manager  # type: ignore

    with reversion.create_revision():
        vorule.is_manager = False
        vorule.save()

    member = CommunityMember.objects.get(pk=member.pk)
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    assert not user.is_manager  # type: ignore


def test_is_manager_upon_create(
    vo: HelmholtzVirtualOrganization,
    vorule_factory: Callable[..., models.VORule],
    member: CommunityMember,
):
    """Test the creation of members upon saving a rule."""

    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.groups.add(vo)
    vorule = vorule_factory(vo=vo, is_manager=True)

    assert user.is_manager  # type: ignore

    # test removal
    vorule.delete()

    member = CommunityMember.objects.get(pk=member.pk)
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    assert not user.is_manager  # type: ignore


def test_multiple_is_manager(
    vorule_factory: Callable[..., models.VORule], member: CommunityMember
):
    """Test having multiple VOs with is_manager=True"""
    vorule1 = vorule_factory(is_manager=True)
    vorule2 = vorule_factory(is_manager=True)

    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    user.groups.add(vorule1.vo, vorule2.vo)
    models.add_manager_from_vo(user, vorule1.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    assert user.is_manager  # type: ignore

    user.groups.remove(vorule1.vo)
    models.remove_manager(user, vorule1.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    assert user.is_manager  # type: ignore

    user.groups.remove(vorule2.vo)
    models.remove_manager(user, vorule2.vo)

    member = CommunityMember.objects.get(pk=member.pk)
    user: HelmholtzUser = member.user.helmholtzuser  # type: ignore
    assert not user.is_manager  # type: ignore


# ----------------------- tests for organization ------------------------------
def test_add_organization(
    vorule: models.VORule,
    member: CommunityMember,
    department: Department,
    user: HelmholtzUser,
):
    """Test adding a member to an organization."""
    with reversion.create_revision():
        vorule.organization = department
        vorule.save()
    user.groups.add(vorule.vo)
    models.add_academicmembership(user, vorule.vo)

    assert member.is_member_of(department)

    user.groups.remove(vorule.vo)
    models.end_academicmembership(user, vorule.vo)

    assert not member.is_member_of(department)


def test_add_organization_upon_save(
    vorule: models.VORule,
    member: CommunityMember,
    department: Department,
    user: HelmholtzUser,
):
    """Test adding a member to an organization when changing the rule."""
    user.groups.add(vorule.vo)
    with reversion.create_revision():
        vorule.organization = department
        vorule.save()

    assert member.is_member_of(department)

    with reversion.create_revision():
        vorule.organization = None
        vorule.save()

    assert not member.is_member_of(department)


def test_add_organization_upon_create(
    vorule_factory: Callable[..., models.VORule],
    vo: HelmholtzVirtualOrganization,
    member: CommunityMember,
    department: Department,
    user: HelmholtzUser,
):
    """Test adding a member to an organization when creating a rule."""
    user.groups.add(vo)
    vorule = vorule_factory(organization=department, vo=vo)

    assert member.is_member_of(department)

    vorule.delete()

    assert not member.is_member_of(department)


def test_switch_to_unit(
    vorule_factory: Callable[..., models.VORule],
    vo_factory: Callable[[], HelmholtzVirtualOrganization],
    member: CommunityMember,
    unit: Unit,
    department: Department,
    user: HelmholtzUser,
):
    """Test switching to a unit"""
    vo1 = vo_factory()
    vo2 = vo_factory()
    user.groups.add(vo1, vo2)

    vorule_factory(vo=vo1, organization=department)

    assert member.is_member_of(department)
    assert member.academicmembership_set.count() == 1  # type: ignore

    vorule2 = vorule_factory(vo=vo2, organization=unit)

    assert member.is_member_of(unit)
    assert member.academicmembership_set.count() == 1  # type: ignore

    vorule2.delete()

    assert member.is_member_of(department)
    assert member.academicmembership_set.count() == 1  # type: ignore


def test_add_another_unit(
    vorule_factory: Callable[..., models.VORule],
    vo_factory: Callable[[], HelmholtzVirtualOrganization],
    member: CommunityMember,
    unit_factory: Callable[[], Unit],
    user: HelmholtzUser,
):
    """Test multiple units"""
    vo1 = vo_factory()
    vo2 = vo_factory()
    unit1 = unit_factory()
    unit2 = unit_factory()
    user.groups.add(vo1, vo2)

    vorule_factory(vo=vo1, organization=unit1)

    assert member.is_member_of(unit1)
    assert member.academicmembership_set.count() == 1  # type: ignore

    vorule2 = vorule_factory(vo=vo2, organization=unit2)

    assert member.is_member_of(unit1)
    assert member.is_member_of(unit2)
    assert member.academicmembership_set.count() == 2  # type: ignore

    vorule2.delete()

    assert member.is_member_of(unit1)
    assert not member.is_member_of(unit2)
    assert len(member.active_memberships) == 1  # type: ignore


def test_existing_membership_create(
    vo: HelmholtzVirtualOrganization,
    vorule_factory: Callable[..., models.VORule],
    member: CommunityMember,
    user: HelmholtzUser,
    institution: Institution,
):
    """Test creating a rule with an existing academic membership."""
    user.groups.add(vo)
    member.academicmembership_set.create(organization=institution)  # type: ignore

    assert member.academicmembership_set.count() == 1  # type: ignore

    vorule = vorule_factory(vo=vo, organization=institution)

    assert member.academicmembership_set.count() == 1  # type: ignore

    vorule.delete()

    assert len(member.active_memberships) == 0  # type: ignore


def test_existing_membership_save(
    vorule: models.VORule,
    member: CommunityMember,
    user: HelmholtzUser,
    institution: Institution,
):
    """Test creating a rule with an existing academic membership."""
    user.groups.add(vorule.vo)
    member.academicmembership_set.create(organization=institution)  # type: ignore

    assert member.academicmembership_set.count() == 1  # type: ignore

    with reversion.create_revision():
        vorule.organization = institution
        vorule.save()

    assert member.academicmembership_set.count() == 1  # type: ignore

    vorule.delete()

    assert len(member.active_memberships) == 0  # type: ignore

<!--
SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC0-1.0
-->

# Changelog

## v0.2.0: Implement django-helmholtz-aai 0.2.0

- update to latest django-helmholtz-aai implementation, see !5

## v0.1.2: Improve admin display

- patch to improve the list display of the django admin

## v0.1.1: Patch for existing memberships

- If an academic membership exists already, we do not update it
- The `HELMHOLTZ_AAI_UPDATE_MEMBERS` settings variable has been set to `True`
  by default

## v0.1.0: Initial release

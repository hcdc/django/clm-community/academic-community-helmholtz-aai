# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""pytest configuration script for academic-community-helmholtz-aai."""

import pytest  # noqa: F401
